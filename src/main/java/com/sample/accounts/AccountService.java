package com.sample.accounts;

import com.sample.exception.UserDuplicatedException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author Taewoo, Kim(Michael) (5zzang@gmail.com)
 * @since 14-09-2015
 */
@Service
@Transactional
@Slf4j  // Lombok에서 지원하는 Annotation으로, log라는 변수가 생성된다.
public class AccountService {
    @Autowired
    private AccountRepository repository;

    @Autowired
    private ModelMapper modelMapper;


    public Account createAccount(AccountDto.Create dto) {
        // old style
//        Account account = new Account();
//        account.setUsername(dto.getUsername());
//        account.setPassword(dto.getPassword());

        // ModelMapper style
        Account account = modelMapper.map(dto, Account.class);

        // TODO 유효한 username인지 판단
        String username = dto.getUsername();
        if (repository.findByUsername(username) != null) {
            log.error("user duplicated exception. {}", username);
            throw new UserDuplicatedException(username);
        }

        // TODO password 해싱
        Date now = new Date();
        account.setJoined(now);
        account.setUpdated(now);

        return repository.save(account);
    }
}
