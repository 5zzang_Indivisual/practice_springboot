package com.sample.accounts;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Date;

/**
 * @author Taewoo, Kim(Michael) (5zzang@gmail.com)
 * @since 11-09-2015
 */
@Entity
@Data
public class Account {
    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String username;

    //@JsonIgnore   // 이 애노테이션을 붙이면 JSON출력시 password는 절대 출력되지 않는다.
    private String password;

    private String email;

    private String fullName;

    @Temporal(TemporalType.TIMESTAMP)
    private Date joined;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;
}
