package com.sample.commons;

import lombok.Data;

import java.util.List;

/**
 * @author Taewoo, Kim(Michael) (5zzang@gmail.com)
 * @since 14-09-2015
 */
@Data
public class ErrorResponse {
    private String message;
    private String code;
    private List<FieldError> errors;

    // TODO
    public static class FieldError {
        private String field;
        private String value;
        private String reason;
    }
}
