package com.sample.exception;

import com.sample.commons.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 모든 Controller에 적용되는 전역 Exception Handler.
 *
 * @author Taewoo, Kim(Michael) (5zzang@gmail.com)
 * @since 22-09-2015
 */
@ControllerAdvice
public class ExceptionHandlers {
//    @ExceptionHandler(UserDuplicatedException.class)
//    public ResponseEntity handleUserDuplicatedException(UserDuplicatedException e) {
//        ErrorResponse errorResponse = new ErrorResponse();
//        errorResponse.setMessage("[" + e.getUsername() + "] 중복된 username 입니다.");
//        errorResponse.setCode("duplicated.username.exception");
//
//        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
//    }
}
