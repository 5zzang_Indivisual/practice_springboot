package com.sample.accounts;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.sample.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;


import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Taewoo, Kim(Michael) (5zzang@gmail.com)
 * @since 14-09-2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@Transactional  // Test클래스에 Transactional을 붙여주면, 테스트후 Rollback을 진행한다.
//@Rollback(false)  // Spring 4.2.x 부터는 Rollback 어노테이션이 Class에도 적용 된다.
public class AccountControllerTest {

    @Autowired
    WebApplicationContext wac;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    AccountService service;

    MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    // TODO 서비스 호출에서 예외 상황을 비동기 콜백으로 처리.
    @Test
    //@Rollback(false)  // 해당 Test는 Rollback 에서 제외됨.
    public void  createAccount() throws Exception {
        AccountDto.Create create = new AccountDto.Create();
        create.setUsername("michael");
        create.setPassword("password");

        ResultActions result = mockMvc.perform(post("/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(create)));

        result.andDo(print());
        result.andExpect(status().isCreated());
        result.andExpect(jsonPath("$.username", is("michael")));
//        result.andExpect(jsonPath("$.username", is("mike")));

        // Check Duplicate
        result = mockMvc.perform(post("/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(create)));

        result.andDo(print());
        result.andExpect(status().isBadRequest());
        result.andExpect(jsonPath("$.code", is("duplicated.username.exception")));
    }

    @Test
    public void createAccount_BadRequest() throws Exception {
        AccountDto.Create create = new AccountDto.Create();
        create.setUsername("  ");
        create.setPassword("1234");

        ResultActions result = mockMvc.perform(post("/accounts")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(create)));

        result.andDo(print());
        result.andExpect(status().isBadRequest());
        result.andExpect(jsonPath("$.code", is("bad.request")));
    }

    @Test
    public void getAccounts() throws Exception {
        AccountDto.Create create = new AccountDto.Create();
        create.setUsername("michael");
        create.setPassword("password");
        service.createAccount(create);

        ResultActions result = mockMvc.perform(get("/accounts"));

        /**
         * {
         *  "content":[{"id":1,"username":"michael","fullName":null,"joined":1442304516606,"updated":1442304516606}]
         *  ,"totalElements":1
         *  ,"totalPages":1
         *  ,"last":true
         *  ,"size":20
         *  ,"number":0
         *  ,"first":true
         *  ,"sort":null
         *  ,"numberOfElements":1
         * }
         */
        result.andDo(print());
        result.andExpect(status().isOk());
    }
}